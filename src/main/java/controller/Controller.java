package controller;

import gameExceptions.GameOverException;
import gameModel.GameModel;
import view.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Controller extends JPanel implements ActionListener {
    private final GameModel gameModel;
    private final View view;
    private final Timer timer;

    public static boolean inGame = false;

    public Controller(GameModel gameModel, View view) {
        this.gameModel = gameModel;
        this.view = view;

        timer = new Timer(1000 / GameModel.speed, this); // секунда/скорость
        timer.start();

        addKeyListener(new KeyBoard());
        setFocusable(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (GameModel.difficulty == 1) {
                GameModel.speed = 9;
                timer.setDelay(1000 / GameModel.speed);
            }
            if (GameModel.difficulty == 2) {
                GameModel.speed = 13;
                timer.setDelay(1000 / GameModel.speed);
            }
            if (GameModel.difficulty == 3) {
                GameModel.speed = 17;
                timer.setDelay(1000 / GameModel.speed);
            }
            if (inGame)
                gameModel.change();
        } catch (GameOverException exception) {
            timer.stop();
            view.showDialogPanel("Game over. Do you want to restart?");
            gameModel.restart();
            inGame = false;
            timer.start();
        }
    }

    public class KeyBoard extends KeyAdapter {
        long currentTimeMillis = System.currentTimeMillis();

        public void keyPressed(KeyEvent event) {
            int key = event.getKeyCode();

            if (key == KeyEvent.VK_SPACE) {
                inGame = true;
            }
            if (key == KeyEvent.VK_W && gameModel.getSnakeDirection() != 2 &&
                    System.currentTimeMillis() - currentTimeMillis > 50) {
                gameModel.setSnakeDirection(0);
                currentTimeMillis = System.currentTimeMillis();
            } else if (key == KeyEvent.VK_S && gameModel.getSnakeDirection() != 0 &&
                    System.currentTimeMillis() - currentTimeMillis > 50) {
                gameModel.setSnakeDirection(2);
                currentTimeMillis = System.currentTimeMillis();
            } else if (key == KeyEvent.VK_A && gameModel.getSnakeDirection() != 1 &&
                    System.currentTimeMillis() - currentTimeMillis > 50) {
                gameModel.setSnakeDirection(3);
                currentTimeMillis = System.currentTimeMillis();
            } else if (key == KeyEvent.VK_D && gameModel.getSnakeDirection() != 3 &&
                    System.currentTimeMillis() - currentTimeMillis > 50) {
                gameModel.setSnakeDirection(1);
                currentTimeMillis = System.currentTimeMillis();
            }
        }
    }
}