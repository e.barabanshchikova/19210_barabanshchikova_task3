package view;

import gameModel.GameModel;

import javax.swing.*;
import java.awt.*;

public class View extends JPanel {
    private final GameModel gameModel;
    private Image snake;
    private Image apple;
    private Image head;

    public View(GameModel gameModel) {
        this.gameModel = gameModel;
        loadImages();
    }

    public void loadImages() {
        ImageIcon iia = new ImageIcon("src/main/Images/food.png");
        apple = iia.getImage();
        ImageIcon iid = new ImageIcon("src/main/Images/snake.png");
        snake = iid.getImage();
        ImageIcon iih = new ImageIcon("src/main/Images/head.png");
        head = iih.getImage();
    }

    public void paint(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.black);
        g.fillRect(0, 0, GameModel.WIDTH * GameModel.SCALE + 15, GameModel.HEIGHT * GameModel.SCALE + 30);

        g.drawImage(apple, gameModel.getApple().posX * GameModel.SCALE, gameModel.getApple().posY * GameModel.SCALE, this);

        for (int l = 1; l < gameModel.getSnakeLength(); l++) {
            g.drawImage(snake, gameModel.getSnake().snakeX[l] * GameModel.SCALE, gameModel.getSnake().snakeY[l] * GameModel.SCALE, this);
            g.drawImage(head, gameModel.getSnake().snakeX[0] * GameModel.SCALE, gameModel.getSnake().snakeY[0] * GameModel.SCALE, this);
        }

        g.setColor(Color.white);
        g.setFont(new Font("Ink Free", Font.BOLD, 40));
        FontMetrics metrics = getFontMetrics(g.getFont());
        g.drawString("Score: " + GameModel.applesEaten, (GameModel.WIDTH * GameModel.SCALE - metrics.stringWidth("Score: " + GameModel.applesEaten)) / 2, g.getFont().getSize());

        g.setFont(new Font("Ink Free", Font.BOLD, 27));
        g.drawString("Difficulty: " + GameModel.difficulty, (GameModel.WIDTH * GameModel.SCALE - metrics.stringWidth("Difficulty: " + GameModel.difficulty)) / 2 + 35, g.getFont().getSize() + 38);

        repaint();
    }

    public void showDialogPanel(String message) {
        JOptionPane.showMessageDialog(null, "\n" + message);
    }
}