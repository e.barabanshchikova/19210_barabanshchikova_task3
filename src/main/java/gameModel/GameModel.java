package gameModel;

import gameExceptions.GameOverException;

public class GameModel {
    public static final int SCALE = 32;
    public static final int WIDTH = 20;
    public static final int HEIGHT = 20;
    public static int speed = 10;
    public static int applesEaten = 0;
    public static int difficulty = 1;

    private final Snake s = new Snake(5, 6, 5, 5);
    private final Apple a = new Apple(Math.abs((int) (Math.random() * GameModel.WIDTH - 1)), Math.abs((int) (Math.random() * GameModel.HEIGHT - 1)));

    public Snake getSnake() {
        return s;
    }

    public int getSnakeDirection() {
        return s.getDirection();
    }

    public void setSnakeDirection(int newDirection) {
        s.setDirection(newDirection);
    }

    public int getSnakeLength() {
        return s.getLength();
    }

    public Apple getApple() {
        return a;
    }

    public void change() {
        s.move();
        if ((s.snakeX[0] == a.posX) && (s.snakeY[0] == a.posY)) {
            a.setRandomPosition();
            s.setLength(s.getLength() + 1);
            applesEaten++;
        }
        for (int l = 1; l < s.getLength(); l++) {
            if ((s.snakeX[l] == a.posX) && (s.snakeY[l] == a.posY)) {
                a.setRandomPosition();
            }
            if ((s.snakeX[0] == s.snakeX[l]) && (s.snakeY[0] == s.snakeY[l])) {
                throw new GameOverException();
            }
        }
    }

    public void restart() {
        s.getNewSnake();
        applesEaten = 0;
        a.setRandomPosition();
    }
}
