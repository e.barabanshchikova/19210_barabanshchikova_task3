package gameModel;

public class Apple {
    public int posY;
    public int posX;

    public Apple(int x, int y) {
        posX = x;
        posY = y;
    }

    public void setRandomPosition() {
        posX = Math.abs((int) (Math.random() * GameModel.WIDTH - 1));
        posY = Math.abs((int) (Math.random() * GameModel.HEIGHT - 1));
    }

}
