package gameModel;

import gameExceptions.GameOverException;

public class Snake {

    private int length;
    private int direction;

    public int[] snakeX = new int[GameModel.WIDTH * GameModel.SCALE];
    public int[] snakeY = new int[GameModel.SCALE * GameModel.HEIGHT];

    public Snake(int x1, int y1, int x2, int y2) {
        length = 2;
        direction = 2;

        snakeX[0] = x1;
        snakeX[1] = x2;
        snakeY[0] = y1;
        snakeY[1] = y2;
    }

    public void getNewSnake() {
        length = 2;
        direction = 2;

        snakeX[0] = 5;
        snakeX[1] = 5;
        snakeY[0] = 6;
        snakeY[1] = 5;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public void move() {
        for (int l = length; l > 0; l--) {
            snakeX[l] = snakeX[l - 1];
            snakeY[l] = snakeY[l - 1];
        }

        //UP
        if (direction == 0) snakeY[0]--;
        //DOWN
        if (direction == 2) snakeY[0]++;
        //RIGHT
        if (direction == 1) snakeX[0]++;
        //LEFT
        if (direction == 3) snakeX[0]--;

        if ((GameModel.difficulty == 1) || (GameModel.difficulty == 2)) {
            if (snakeY[0] > GameModel.HEIGHT - 1) snakeY[0] = 0;
            if (snakeY[0] < 0) snakeY[0] = GameModel.HEIGHT - 1;
            if (snakeX[0] > GameModel.WIDTH - 1) snakeX[0] = 0;
            if (snakeX[0] < 0) snakeX[0] = GameModel.WIDTH - 1;
        } else {
            if (snakeY[0] > GameModel.HEIGHT - 1 ||
                    snakeY[0] < 0 ||
                    snakeX[0] > GameModel.WIDTH - 1 ||
                    snakeX[0] < 0)
                throw new GameOverException();
        }
    }
}
