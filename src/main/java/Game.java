import controller.Controller;
import gameModel.GameModel;
import view.View;

import javax.swing.*;

public class Game {
    public static void main(String[] args) {

        JFrame jFrame;
        GameModel gameModel = new GameModel();

        jFrame = new JFrame("Snake");
        jFrame.setSize(GameModel.WIDTH * GameModel.SCALE + 14, GameModel.HEIGHT * GameModel.SCALE + 60);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setResizable(false);
        jFrame.setLocationRelativeTo(null);

        View view = new View(gameModel);
        Controller controller = new Controller(gameModel, view);

        JMenuBar menuBar = new JMenuBar();

        jFrame.setJMenuBar(menuBar);
        JMenu editMenu = new JMenu("Difficulty");
        menuBar.add(editMenu);
        JMenuItem easyItem = new JMenuItem("Easy");
        editMenu.add(easyItem);
        JMenuItem mediumItem = new JMenuItem("Medium");
        editMenu.add(mediumItem);
        JMenuItem hardItem = new JMenuItem("Hard");
        editMenu.add(hardItem);

        easyItem.addActionListener(e -> {
            Controller.inGame = false;
            gameModel.restart();
            GameModel.difficulty = 1;
        });

        mediumItem.addActionListener(e -> {
            Controller.inGame = false;
            gameModel.restart();
            GameModel.difficulty = 2;
        });

        hardItem.addActionListener(e -> {
            Controller.inGame = false;
            gameModel.restart();
            GameModel.difficulty = 3;
        });

        jFrame.add(controller);
        jFrame.add(view);
        jFrame.setVisible(true);
    }
}
