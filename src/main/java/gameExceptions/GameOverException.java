package gameExceptions;

public class GameOverException extends RuntimeException {
    public GameOverException() {
        super("Game's over!");
    }
}
